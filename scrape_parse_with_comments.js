var Nightmare = require('nightmare')
var cheerio = require('cheerio');
var vo = require('vo')
var fs = require('fs');


moment = require('moment');
const MomentRange = require('moment-range');
const momentrange = MomentRange.extendMoment(moment)

var start = moment('2017-04-18');
var end = moment('2017-04-24');
var pageinpixels = 40000;

var output_json = [];

vo(run)(function(err, result) {
    if (err) throw err

    var $ = cheerio.load(result);

    $('.fbUserContent').each(function(){
        var time_of_post = $(this).find('._5pcq').text();
        var time_of_post_in_moment = moment(time_of_post, ["MMMM DD", "H", "mm"]).format('YYYY-MM-DD');

        var range = moment().range(start,end);
        date = moment(time_of_post_in_moment);
        var time_of_post_in_moment_contains_start_end = range.contains(date);

        if (time_of_post_in_moment_contains_start_end) {

            facebook_post = {};
            facebook_post['time_current'] = time_of_post_in_moment;
            facebook_post['time_old'] = $(this).find('.timestampContent').text();
            facebook_post['datetime'] = $(this).find('._5pcq > abbr').attr('title');
            facebook_post['profile'] = $(this).find('.fwb > a').text();
            facebook_post['profile_link'] = $(this).find('.fwb > a').attr('href');
            facebook_post['comments'] = {};
            $(this).find('.UFICommentContentBlock').each(function (i, elem) {
                facebook_post['comments'][i] = {}
                facebook_post['comments'][i]['name'] = $(this).find('.UFICommentActorName').text()
                facebook_post['comments'][i]['name_url'] = $(this).find('.UFICommentActorName').attr('href')
                facebook_post['comments'][i]['comment'] = $(this).find('.UFICommentBody').text()
                facebook_post['comments'][i]['datetime'] = $(this).find('.livetimestamp').attr('title')
                facebook_post['comments'][i]['reaction'] = $(this).find('.UFICommentLikeButton').attr('aria-label')
                if ($(this).find('.UFICommentLikeButton').attr('href')) {
                    facebook_post['comments'][i]['reaction_link'] = "https://www.facebook.com" + $(this).find('.UFICommentLikeButton').attr('href')
                }
            });
            facebook_post['shares'] = [];
            $(this).find('._3emk').each(function (i, elem) {
                facebook_post['shares'].push($(this).attr('aria-label'));
            });

            $(this).find('._4arz').each(function (i, elem) {
                facebook_post['shares'].push("All:" + $(this).text());
            });

            facebook_post['who_shared'] = {};
            $(this).find('._3emk').each(function (i, elem) {
                facebook_post['who_shared'][$(this).attr('aria-label')] = "https://www.facebook.com"+ $(this).attr('href');
            });

            if ($(this).find('._4-eo._2t9n').attr('href')) {
                facebook_post['post_img_url'] = "https://www.facebook.com" + $(this).find('._4-eo._2t9n').attr('href');
            }

            facebook_post['photo_url2'] = $(this).find('._4-eo._2t9n').attr('data-ploi');
            facebook_post['share-properties'] = $(this).find('.UFIShareLink').text();

            output_json.push(facebook_post);
        }
    })

    fs.writeFile("scrape_parse_with_comments.json", JSON.stringify(output_json, null, 4), function(err) {
        if(err) { return console.log(err); } })

})


function *run() {
    var nightmare = Nightmare({show:true});
    yield nightmare.goto('https://www.facebook.com/');
    yield nightmare.type('#email ', 'viahost@yandex.ru');
    yield nightmare.type('#pass ', '1qaz2wsx3edc');
    yield nightmare.click('#loginbutton > #u_0_q');
    yield nightmare.wait(4000);

    var previousHeight, currentHeight=0;

    while(currentHeight < pageinpixels) {
        previousHeight = currentHeight;
        var currentHeight = yield nightmare.evaluate(function() {
            return document.body.scrollHeight;
        });

        yield nightmare.scrollTo(currentHeight, 0)
            .wait(1000);

        var body = yield nightmare
            .goto('https://www.facebook.com/pg/EnMarche/posts/')
            .wait(1000)
            .evaluate(function()
            {
                return document.getElementsByTagName('body')[0].innerHTML;
            })
            .then(function(result) {

                var $ = cheerio.load(result);
                $('.UFIPagerLink').each(function()
                {
                    var xxxx = $(this).find('._4qba').text();

                    return nightmare
                        .click('.UFIPagerLink >._4qba')
                        .wait(3000)
                })

                return nightmare
                    .evaluate(function()
                    {
                        return document.getElementsByTagName('body')[0].innerHTML;
                    })
            })
    }

    yield nightmare.end();
    return body
};